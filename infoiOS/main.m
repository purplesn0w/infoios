//
//  main.m
//  infoiOS
//
//  Created by Aiden Bradley Albert on 3/26/18.
//  Copyright (c) 2018 Aiden Bradley Albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
