//
//  ViewController.m
//  infoiOS
//
//  Created by Aiden Bradley Albert on 3/26/18.
//  Copyright (c) 2018 Aiden Bradley Albert. All rights reserved.
//

#import "ViewController.h"
#import <sys/sysctl.h>
#import <dlfcn.h>
#import <sys/utsname.h>
//#import "DSEntitlementUtilities.h"
//#import "DSUsageLogLine.h"
//#import "DSGeneralLogFile.h"
#import "MobileGestalt.h"
#import "BSPlatform.h"
#import <MessageUI/MessageUI.h>
#define DEBUG_BUILD 0
@interface ViewController ()

@end
//Credit to Morpheus__ and many others for code used in this application!

char *_CFNumberToString (CFNumberRef 	Num)
{
    char *num = malloc(128); //CFNumberGetByteSize(Num));
    
    // Should check return value. Meh
    CFNumberGetValue(Num,  // CFNumberRef number
                     CFNumberGetType(Num) , // CFNumberType theType
                     num); // void *valuePtr);
    
    char *returned = calloc (128,1); // MOOOORE than enough
    switch (CFNumberGetType(Num))
    {
        case kCFNumberSInt8Type: //  = 1,
            sprintf (returned, "%d", num[0]);
            break;
        case kCFNumberSInt16Type:  // = 2,
            sprintf (returned, "%d", *((short *) num));
            break;
        case kCFNumberSInt32Type: // =3,
            sprintf (returned, "%d", *((int32_t *) num));
            break;
        case kCFNumberSInt64Type: // = 4,
            sprintf (returned, "%lld", *((int64_t *) num));
            break;
        default:
            return ("?");
            
#if 0
            /* @TODO, at some point.. Don't get too many values of these types in MG
             kCFNumberFloat32Type = 5,
             kCFNumberFloat64Type = 6,   /* 64-bit IEEE 754 */
            /* Basic C types */
            kCFNumberCharType = 7,
            kCFNumberShortType = 8,
            kCFNumberIntType = 9,
            kCFNumberLongType = 10,
            kCFNumberLongLongType = 11,
            kCFNumberFloatType = 12,
            kCFNumberDoubleType = 13,
            
#endif
    }
    return (returned);
    
}
CFTypeRef (*MGCopyAnswer_func)(CFStringRef question);

const char *gestaltAnswer (char *Question)
{
    
    
    if (!MGCopyAnswer_func)
    {
        void *gestaltLib = dlopen ("/usr/lib/libMobileGestalt.dylib", RTLD_LAZY);
        
        if (!gestaltLib)  { /* error */ return NULL;}
        
        // Found library, now find symbol
        MGCopyAnswer_func = dlsym (gestaltLib, "MGCopyAnswer");
        
        if (!MGCopyAnswer_func)  { /* error */ return NULL;}
        
    } // end if !MGCopy..
    
    // Still here, so gestalt is at our disposal!
    
    
    CFStringRef     q = CFStringCreateWithCString(NULL,
                                                  Question,
                                                  kCFStringEncodingASCII);
    
    CFTypeRef answer = MGCopyAnswer_func(q);
    
    if (!answer)
    {
        // Note that lack of an answer here doesn't necessarily mean that
        // the key was not found - it could also imply a permission problem
        // (q.v. MobileGestalt's "protected keys"
        return(NULL);
    }
    
    const char *out = NULL;
    
    // CopyAnswer returns a CF object. Which, we have to figure out dynamically
    // (although you can use one of Get[Bool/Float/SInt[32/64]] variants as well)
    CFTypeID typeId = CFGetTypeID (answer);
    
    if (typeId == CFStringGetTypeID())
    {
        out = CFStringGetCStringPtr(answer, kCFStringEncodingASCII);
    }
    
    if (typeId == CFBooleanGetTypeID())
    {
        out = (CFBooleanGetValue(answer) ? "true" : "false");
    }
    if (typeId == CFArrayGetTypeID())
    {
        //@TODO (my plane is landing in a bit :-)
        out = "Array";
    }
    if (typeId == CFDictionaryGetTypeID())
    {
        CFDataRef xml = CFPropertyListCreateXMLData(kCFAllocatorDefault,
                                                    (CFPropertyListRef)answer);
        if (xml) { out =  (const char *) CFDataGetBytePtr(xml); }
        // should really CFRelease(xml);
        
        
    } // dict
    if (typeId == CFNumberGetTypeID())
    {
        
        
        out = _CFNumberToString (answer);
        
    }
    if (typeId == CFDataGetTypeID())
    {
        CFIndex len = CFDataGetLength(answer);
        const UInt8 *data = CFDataGetBytePtr(answer);
        
        // We want to dump this to hex:
        char *returned = (char *) calloc (len * 3,1);
        
        int i = 0;
        for (i = 0 ; i < len; i++)
        {
            sprintf(returned + strlen(returned), "%02X ", data[i]);
        }
        
        return (returned);
    }
    
    
    return (out);
    
    
}


//
// Base64 support
//
char table[] =
{
    'A', 'B', 'C', 'D','E','F','G','H','I', 'J', 'K', 'L', 'M',
    'N', 'O', 'P', 'Q','R','S','T','U','V', 'W', 'X', 'Y', 'Z',
    'a', 'b', 'c', 'd','e','f','g','h','i', 'j', 'k', 'l', 'm',
    'n', 'o', 'p', 'q','r','s','t','u','v', 'w', 'x', 'y', 'z',
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
    '+',
    '/'
    
};






@implementation SimpleTableViewController
{
    NSArray *questionsPlist;
    NSArray *images;
    NSArray *dataPlist;
    NSString *labelToPass;
    NSString *dataToPass;
}
- (BOOL) connectedToInternet
{
    NSURL *requestURL = [NSURL URLWithString:@"http://www.google.com"];
    NSData *data = [NSData dataWithContentsOfURL:requestURL];
    
    return ([data bytes]>0) ? YES : NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    success = [[NSMutableArray alloc] init];
    fail = [[NSMutableArray alloc] init];
//    if ([self connectedToInternet]) {
//        NSData *questionsPlist2 = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:@"https://raw.githubusercontent.com/GithubPagesStuff/GBUtils/master/questions.plist"]];
//        NSData *dataPlist2 = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:@"https://raw.githubusercontent.com/GithubPagesStuff/GBUtils/master/data.plist"]];
//        dataPlist = [[NSArray alloc]initWithContentsOfURL:[NSURL URLWithString:@"https://raw.githubusercontent.com/GithubPagesStuff/GBUtils/master/answers.plist"]];
//        questionsPlist = [[NSArray alloc]initWithContentsOfURL:[NSURL URLWithString:@"https://raw.githubusercontent.com/GithubPagesStuff/GBUtils/master/questions.plist"]];
//        //        NSLog(questionsPlist);
//        //        NSLog(dataPlist);
//        if (dataPlist.count != questionsPlist.count) {
//            NSString *sourcePath = [[NSBundle mainBundle] pathForResource:@"questions"    ofType:@"plist"];
//            NSString *sourcePath2 = [[NSBundle mainBundle] pathForResource:@"data"    ofType:@"plist"];
//            dataPlist = [NSArray arrayWithContentsOfFile:sourcePath2];
//            questionsPlist = [NSArray arrayWithContentsOfFile:sourcePath];
//        } else {
//            NSString *sourcePath = [[NSBundle mainBundle] pathForResource:@"questions"    ofType:@"plist"];
//            NSString *sourcePath2 = [[NSBundle mainBundle] pathForResource:@"data"    ofType:@"plist"];
//            dataPlist = [NSArray arrayWithContentsOfFile:sourcePath2];
//            questionsPlist = [NSArray arrayWithContentsOfFile:sourcePath];
//            
//            
//        }
//    }
    
    // Do any additional setup after loading the view, typically from a nib.
        questionsPlist = [NSArray arrayWithObjects:@"OS", @"OS Version",@"Device Type",@"Model",@"Model Name", @"Cores",@"Kernel",@"BoardCFG", @"OS ID", @"Name",@"Kernel Ver.", @"Fusing",@"Resolution",@"Install Type", nil];
    NSArray *arrai = [NSArray arrayWithObjects:@"hw.machine",@"kern.ostype",@"kern.version",@"hw.model",@"kern.osversion",@"kern.osrevision", @"hw.memsize",@"hw.l1icachesize", @"hw.l2cachesize",@"", nil];
    NSString *installType = [[NSString alloc] init];
    NSMutableArray *arraiii = [[NSMutableArray alloc] init];
//        images = [NSArray arrayWithObjects:[UIImage imageNamed:@"cydia.jpg"],[UIImage imageNamed:@"eraser.png"], nil];
    for (NSString *curString in arrai) {
        const char *curStriing = [curString cStringUsingEncoding:NSUTF8StringEncoding];
        size_t size;
    sysctlbyname([curString cStringUsingEncoding:NSUTF8StringEncoding], NULL, &size, NULL, 0);
    char *model = malloc(size);
    sysctlbyname([curString cStringUsingEncoding:NSUTF8StringEncoding], model, &size, NULL, 0);
    NSString *deviceModel = [NSString stringWithCString:model encoding:NSUTF8StringEncoding];
    [arraiii addObject:deviceModel];
//    [DSEntitlementUtilities currentProcessHasEntitlement:@"task_for_pid-allow"];
    }
    NSString *fusingStatus = [[NSString alloc] init];
//    [[[DSGeneralLogCollector latestRootLog] restoreDate] date];
    NSNumber *batLVL = [[NSNumber alloc] initWithInt:1];
    if ([[BSPlatform sharedInstance] isInternalInstall]) {
        installType = @"AppleInternal";
    } else {
    if ([[BSPlatform sharedInstance] isCarrierInstall]) {
    installType = @"CarrierOS";
    } else {
    if ([[BSPlatform sharedInstance] isDeveloperInstall]) {
    installType = @"Developer";
    } else {
    installType = @"Normal";
    }
    }
    }
//    NSNumber *nn = [[NSNumber alloc]initWithInt:[[[BSPlatform sharedInstance] uniqueDeviceIdentifier] integerValue]];
    NSLog([[BSPlatform sharedInstance] productHardwareModelName]);
    NSLog([NSString stringWithUTF8String:gestaltAnswer("main-screen-height")]);
    NSString *screenH = [NSString stringWithUTF8String:gestaltAnswer("main-screen-height")];
    NSString *screenww = [screenH stringByAppendingString:@"x"];
    NSString *screenW = [screenww stringByAppendingString:[NSString stringWithUTF8String:gestaltAnswer("main-screen-width")]];
    NSString *plistStr = [NSString stringWithUTF8String:gestaltAnswer("SigningFuse")];
    if ([plistStr  isEqual: @"true"]) {
        fusingStatus = @"Production";
    } else {
        fusingStatus = @"Internal";
    }
    NSLog(@"hi");
        NSLog(plistStr);
    
    NSString *sandboxpath = [[NSBundle mainBundle] bundlePath];
NSString *sandboxpath2 = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"gestaltkeys.txt"];
   [plistStr writeToFile:sandboxpath2 atomically:true encoding:NSUTF8StringEncoding error:nil];
    NSString *batLVL2 = batLVL.stringValue;
    NSNumber *cores = [[NSNumber alloc] initWithInt:[[NSProcessInfo processInfo] activeProcessorCount]];
    NSString *coresx = cores.stringValue;
    NSError *error;
    NSMutableArray *arraii = [[NSMutableArray alloc] initWithObjects:[[UIDevice currentDevice] systemName], [[UIDevice currentDevice] systemVersion],[[UIDevice currentDevice] model],[arraiii objectAtIndex:0],[[BSPlatform sharedInstance] productHardwareModelName],coresx, [arraiii objectAtIndex:1], [arraiii objectAtIndex:3],[arraiii objectAtIndex:4],[[UIDevice currentDevice]name],[arraiii objectAtIndex:2], fusingStatus,screenW,installType, nil];
//NSData *xmlData = [NSPropertyListSerialization dataFromPropertyList:plistStr format:NSPropertyListXMLFormat_v1_0
//                                       errorDescription:&error];//
//                                       //  NSArray *processes = [[FBProcessManager sharedInstance] allProcesses];
//                                       NSPropertyListFormat format;
//[xmlData writeToFile:sandboxpath2 atomically:false];
//NSMutableArray *marray = [[NSMutableArray alloc] initWithContentsOfFile:sandboxpath2];
//NSLog([marray description]);
//NSDictionary* plist = [NSPropertyListSerialization propertyListWithData:xmlData options:NSPropertyListImmutable format:&format error:&error];
//NSNumber *tdc = [[NSNumber alloc] initWithInt:[plist objectForKey:@"TotalDataCapacity"]];
//NSLog(tdc.stringValue);
//NSLog([plist objectForKey:@"TotalDataCapacity"]);
//NSLog([plist objectForKey:@"TotalDataAvailable"]);
//NSLog([plist allKeys]);
NSMutableArray *yourPlist = [[NSArray alloc] init];
//[yourPlist addObject:plistx];
printf(gestaltAnswer("BootNonce"));
        dataPlist = [[NSArray alloc] initWithArray:arraii];
    
    if (0 == 1) {
        NSString *sandboxpathx2 = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"gestaltkeys.txt"];
        NSString *str = [[NSString alloc] init];
        str = @"hi";
NSString *keyString = [NSString stringWithContentsOfFile:[[NSBundle mainBundle]
                                                            pathForResource:@"gestaltkeys" ofType:@"txt"]];
           NSArray *keys = [keyString componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];

        NSNumber *keycount = [[NSNumber alloc] initWithInt:[keys count]];
            NSLog(keycount.stringValue);
            
            NSLog(@"string");
            for (NSString *key in keys) {
            NSUInteger *keyc = [keys count] - 1;
                if ([key isEqual:[keys objectAtIndex:keyc]]) {
                [success writeToFile:[[NSBundle mainBundle]
                                                            pathForResource:@"gestaltz" ofType:@"txt"] atomically:true];
                NSLog([fail description]);
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:fail];
                

//                NSLog([success description]);
                }
                const char *cKey = [key cStringUsingEncoding:NSUTF8StringEncoding];
//                id *MGAnswer = MGCopyAnswer(CFSTR(cKey));
                const char *answer = gestaltAnswer(cKey);
                if (answer != NULL) {
                NSString *str = [key stringByAppendingString:@":"];
                NSString *str2 = [str stringByAppendingString:@"Success."];
                [success addObject:key];
//                NSLog(str2);
                } else {
                NSString *str = [key stringByAppendingString:@":Failed."];
                [fail addObject:key];
//                NSLog(str);
                }
                
            }
        
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [questionsPlist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"iCell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    UILabel *l1 = (UILabel *)[cell.contentView viewWithTag:3];
    UILabel *l2 = (UILabel *)[cell.contentView viewWithTag:2];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    if (dataPlist.count != questionsPlist.count) {
        NSString *dpC = @"dataPlist.count::";
        NSNumber *dpcc = [[NSNumber alloc] initWithInt:dataPlist.count];
        NSString *dpC2 = [dpC stringByAppendingString:dpcc.stringValue];
        NSLog(dpC2);
                NSString *qpc = @"questionsPlist.count::";
        NSNumber *qpcc = [[NSNumber alloc] initWithInt:questionsPlist.count];
        NSString *qpc2 = [qpc stringByAppendingString:dpcc.stringValue];
        NSLog(qpc2);
        l1.text = @"Broken.";
    } else {
        l1.text = [questionsPlist objectAtIndex:indexPath.row];
        l2.text = [dataPlist objectAtIndex:indexPath.row];
    }
    //        cell.imageView.image = [UIImage imageNamed:@"cydia.jpg"];
    return cell;
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowDetail"]) {
        //Do something
    }
    
}


//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//
//{
//    
//    if (dataPlist.count != questionsPlist.count) {
//        
//    } else {
//        labelToPass = [questionsPlist objectAtIndex:indexPath.row];
//        dataToPass = [dataPlist objectAtIndex:indexPath.row];
//        NSString * storyboardIdentifier = @"Main";// for example "Main.storyboard" then you have to take only "Main"
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardIdentifier bundle: [NSBundle mainBundle]];
//        HelpViewController *UIVC = [storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
//        
//        UIVC.Label = labelToPass;
//        UIVC.Body = dataToPass;
//        [self presentViewController:UIVC animated:NO completion:nil];
//    }
//}


@end
