//
//  AppDelegate.h
//  infoiOS
//
//  Created by Aiden Bradley Albert on 3/26/18.
//  Copyright (c) 2018 Aiden Bradley Albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

